#include "simpletools.h"                            // Include simpletools
#include "adcDCpropab.h"                            // Include adcDCpropab

//Used Pins: 0 (ped. flashing LED), 1 (push button), LED 27 on parallax (traffic warning light), 8 to 15 (countdown timer)

//maximum 8 cogs on parallax
//Inputs Cog Runs
//void Program1(void* par);  //cog run for microphone
//void Program2(void *par);  //cog run for push button

//Outputs Cog Runs
void LED_Flash();          // get ready for cog run on the ped. LED flashing; refer to cog 3 code below
void Screen();             // get ready for cog run on the screen display; refer to cog 4 code below
void Traffic_Light();      // get ready for cog run on the traffic warning light; refer to cog 5 code below
void Screen_cars();        // get ready for cog run on the screen display; refer to cog 6 code below

int n[] = {0b11110100, 0b11110111, 0b11000100, // #9 - #7 in binary on decimal display
           0b01110111, 0b01110110, 0b10110100, // #6 - #4 in binary on decimal display
           0b11010110, 0b11010011, 0b10000100, // #3 - #1 in binary on decimal display
           0b11100111};                        // #0 in binary on decimal display
//set inputs variables and pins           
int button = input(1);                          // set pin 2 to push button input for ped. crossing
float force;                                    // Voltage variables based on force/pressure on force sensor
force = adc_volts(3);                           // set A/D 3 to read force sensor condition

int main()                                    
{
  //unsigned int stack[40+40];
  
  //cogstart(Program, NULL, stack, sizeof(stack));    //starts the cogs

  //both inputs must be activated in order to run the following
  //force > 1 is only an assumption of sensing the level of pressure, simulating a car is crossing or stop on the sensor
  if(button == 1 && force > 1)
  {
    //max loop = 10 from 0 to 9 decimal display; increment by 1 every time it loops
    for (int i=0; i < 10; ++i)   // the actual counter attached to the breadboard still shows the count down from 9 to 0.                                        
    {
      cog_run(Traffic_Light, 128);          // start this cog to stop all traffic before pedestrian crossing
          
      set_directions(15, 8, 0b11111111);    // Pin15, Pin14, Pin13, ..., Pin7, Pin8 are outputs                                       
      pause(3000);                          // pause 3 seconds assure all traffic stop
        
      //max loop = 10 from 0 to 9 decimal display; increment by 1 every time it loops
      //for (int i=0; i < 10; ++i)   // the actual counter attached to the breadboard still shows the count down from 9 to 0.
    
      cog_run(LED_Flash, 128);   // run cog- LED starts flashing for pedestrian crossing
      
      set_outputs(15,8,n[i]);    // take the array binary to the counter display on breadboard
      
      cog_run(Screen, 128);      // run cog- Screen shows status of Counter and LED
      
      pause(1000);               // pause 1 second
    }
  }
  // Ideally the else-statement below is not needed in real life for saving memories in the system.
  // Implementing this else-statement is only for the purpose of this lab project
  // to make a constrast display of what happens if only the force sensor is pressed.
  else   // this is (force > 100); cars passing the force sensor but no pedestrian pressed the button
  {
    cog_run(Screen_cars, 128);      // run cog- Screen shows status of Counter and LED
  }  
}

//cog 2 code
//void Program2(void *par)                          //pushbuttons labeled with "b"
                                                  //b5 shifts the buttons pitches
//{
  //int button = input(1); //pushbutton, pin 1 is assigned to the input value for the button
  
  //program 2 works in cog2
  //if(button == 1)     
  //{
    //program 2 if button pressed run program with led for *blank* seconds with print to terminal
    
    //set_directions(15, 8, 0b11111111);          // Pin15, Pin14, Pin13, ..., Pin7, Pin8 are outputs                                       
    //pause(1000);                                // pause 1 second
        
    //max loop = 10 from 0 to 9 decimal display; increment by 1 every time it loops
    //for (int i=0; i < 10; ++i)   // the actual counter attached to the breadboard still shows the count down from 9 to 0.
    //{
      //cog_run(LED_Flash, 128);   // run cog- LED starts flashing for pedestrian crossing
      
      //set_outputs(15,8,n[i]);    // take the array binary to the counter display on breadboard
      
      //cog_run(Screen, 128);      // run cog- Screen shows status of Counter, LED (pedestrian), traffic warning light
      
      //pause(1000);               // pause 1 second
    //}             
  //}      
//}

//cog 3 code
void LED_Flash()
{
    high(0);         // LED ON, pin 0
    pause(200);      // Wait 0.2 second
    low(0);          // LED OFF, pin 0
    pause(100);      // Wait another 0.1 second
}

//cog 4 code
void Screen()
{
  if(i == 0) //this i is the loop count in the FOR loop
  {
    print("Counter ON and starts countdown.\n");//show status of countdown timer on screen
    print("Traffic warning light ON.\n");    //show status of traffic warning light
    print("LED Flashing.\n\n");                 //show status of LED; flashing begins
  }
  else if(i == 5)
  {
    print("Counter ON and 5 seconds left.\n");  //show status of countdown timer on screen
    print("Traffic warning light ON.\n");       //show status of traffic warning light
    print("LED Flashing.\n\n");                 //show status of LED; remains flashing
  }
  else // i = 9
  {
    print("Counter OFF.\n");                    //show status of countdown timer on screen
    print("Traffic warning light OFF.\n");      //show status of traffic warning light
    print("LED OFF.\n");                        //show status of LED on screen; stop flashing
  }
}

//cog 5 code
void Traffic_Light()
{
  pause(1000);                                      // Wait 1 s for Terminal app
  adc_init(21, 20, 19, 18);                         // CS=21, SCL=20, DO=19, DI=18

  //float force;                                      // Voltage variables based on force/pressure on force sensor

  //force = adc_volts(3);                             // Check A/D 3
  
  //if statment is used as one of the input options based on the project FSM
  //if(force > 1)                                   
  
    pause(200);                                      // wait 0.2 second
    
    high(27);                                       // turn on traffic warning light
    pause(500);                                     // have the LED on for 0.5 second
    low(27);                                        // turn off traffic warning light
    pause(300);                                     // have the LED off for 0.5 second
    putChar(HOME);
    print("Traffic warning light on.");        // Display status on screen
   
    pause(100);  
}
//cog 6 code
void Screen_cars() 
{
  print("Cars passing by sensor only, ped. push button not pressed.\n") //show status of only cars passing by on screen.
}