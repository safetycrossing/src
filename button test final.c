#include "simpletools.h"                            // Include simpletools
#include "adcDCpropab.h"                            // Include adcDCpropab

// THIS AN EDITED VERSION OF THE ORIGINAL CODE. 


void LED_Flash();         
void Traffic_Light();      
int n[] = {0b11110100, 0b11110111, 0b11000100, // #9 - #7 in binary on decimal display
           0b01110111, 0b01110110, 0b10110100, // #6 - #4 in binary on decimal display
           0b11010110, 0b11010011, 0b10000100, // #3 - #1 in binary on decimal display
           0b11100111, 0b00000000};            // #0 in binary on decimal display and turns all segment off      

int button;                                    
int force;                                     
int main()                                    
{
  while(1)
  {
    button = input(2);                          // set pin 2 to push button input for ped. crossing
      force = input(1);
    if ( ((button == 1) && (force == 1)) || ((button == 1) && (force == 0))) //THIS IS THE MAIN THING THAT CHANGED.
    {
          cog_run(Traffic_Light, 128);          // start this cog to stop all traffic before pedestrian crossing        
          pause(3000);        // wait 3 seconds to ensure traffic has stopped before ped. crossing.        
          cog_run(LED_Flash, 128);              // run cog- LED starts flashing for pedestrian crossing        

           for (int i = 0; i < 11; i++)          // the actual counter attached to the breadboard still shows the countdown from 9 to 0.                                        
          {                          
            set_directions(15, 8, 0b11111111);  // Pin15, Pin14, Pin13, ..., Pin7, Pin8 are outputs
            set_outputs(15,8,n[i]);             // take the array binary to the counter display on breadboard                   

            if (i == 0)                         // this i is the same counter from the FOR loop.
            {
              print("Counter ON and starts countdown.\n");//show status of countdown timer on screen
              print("Traffic warning light ON.\n");       //show status of traffic warning light
              print("LED Flashing.\n\n");                 //show status of LED; flashing begins
            }

            else if((i > 0) && (i <= 9))              // under these conditions, no printing is needed to show on the screen. 
            {
                 // No change at the outputs; countdown timer counting, LED ped. flashing, and traffic warning light flashing
            }

            else // the following prints happen only when i == 10; maximum i counter is 10, refer to FOR loop setting above
            {
              print("Counter OFF.\n");                    //show status of countdown timer on screen
              print("Traffic warning light OFF.\n");      //show status of traffic warning light
              print("LED OFF.\n\n");                      //show status of LED on screen; stop   flashing
            }          
             pause(1000);           // pause 1 second
            } 
          }            
      else
      {
          // condition when button == 0 and force < 4; no cars and no ped, assume ground vibration exists
      }                
  }  
}
//cog 1 code
void LED_Flash()
{
  for (int k = 0; k < 10; k++)
  {
     high(0);         // LED ON, pin 0
     pause(500);      // Wait 0.5 second
     low(0);          // LED OFF, pin 0
     pause(500);      // Wait another 0.5 second 
  }  
}
void Traffic_Light()
{  
  for (int j = 0; j < 17; j++)   // use j as another counter for the traffic warning flashing time.
 {
    high(27);                                       // turn on traffic warning light
    pause(500);                                     // have the LED on for 0.5 second
    low(27);                                        // turn off traffic warning light
    pause(500);                                     // have the LED off for 0.5 second
  }  
}